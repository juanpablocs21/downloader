<?php
set_time_limit(0);
require 'config.php';
require 'libs/helpers.php';

$hash = @$_GET['hash'];
$name = @$_GET['name'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Download... MP3</title>
    <meta content="noarchive, noindex" name="robots" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <style>
      *{margin:0;padding:0;box-sizing:border-box;}
    body{font-size:12px;font-family: verdana;}
    #preloading{height:230px;width:350px;margin:50px auto;}
    </style>
</head>
<body>
  
  <div id="preloading">
     <h1 style="color:#6490E0;font-size:27px;">Generando descarga ..</h1>
     <font style="font:13px Arial; letter-spacing:0.02em;">La descarga se iniciar&aacute; automaticamente..<br>
     Si la descarga no inicia, haga <b>&raquo; <a style="color:#F00;" href="jascript:window.location.reload();">Clic Aqui</a> &laquo;</b></font>
     <br><br>
     Descargando ..
     <br>
        <img src="http://i.imgur.com/jdMej5u.gif" width="326" height="14" style="margin-bottom:3px;">
  </div>


  <script>
    //scripting
    var http = new XMLHttpRequest();
        http.open("POST", "fetch.php", true);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.onreadystatechange = function() {
          if(http.readyState == 4 && http.status == 200) {
            var json = JSON.parse(http.responseText);
            console.log(json);
          }
        }
        http.send("hash=<?=$hash?>&name=<?=urlencode($name)?>&<?=time()?>");
  </script>
</body>
</html>