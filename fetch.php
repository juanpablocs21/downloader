<?php
set_time_limit(0);
require 'config.php';
require 'libs/helpers.php';

$hash = @$_POST['hash'];
$name = @$_POST['name'];

if(empty($hash)) die(json(['error'=>true, 'message'=>'method not permited']));

$data = my_data($hash);

$connect  = new MongoClient();
$database   = $connect->genteflow;
$table_mp3  = $database->mp3;

$result = $table_mp3->find($data)->getNext();


if(!empty($result['file']) && $result!=='-'){

  $results = [
    'mp3'         => generate_secure_hash($result['file'], $name), 
    'size'        => _filesize((int)$result['size']),
    'downloads'   => $result['downloads']
  ];
  print json(['error'=>false, 'message'=>'fetch correct', 'result'=>$results]);

}else{

  $post = array(
    'id'    => $data['id'],
    'source'=> $data['source'],
    'server'=> 1,
  );
  $host_server = "http://s{$post['server']}.mp3yox.com";
  $server = "$host_server/copy.php";
  $r = json_decode(copy_mp3_server($server, $post));

  if(@$r['save_mongo']){
    $results = [
      'mp3'         => $host_server.'/'.$r['url'].''.generate_secure_hash($r['url'], $name), 
      'size'        => $r['size'],
      'downloads'   => $result['downloads']
    ];
    
    $table_mp3->insert([
      'file' => $r['url'],
      'id' => $data['id'],
      'source' => $data['source'],
      'name' => $name,
      'name' => $name,
      'date' => '',
      'size' => $r['size'],
      'downloads'=>1,
      'hits'=>1
    ]);

    print json(['error'=>false, 'message'=>'fetch correct', 'result'=>$results]);
  }else{
    if($r['status']){
      $results = [
        'mp3'     => $r['url'],
        'size'    => $r['size'],
        'downloads'=> 0
      ];
      print json(['error'=>false, 'message'=>'fetch correct', 'result'=>$results]);
    }else{
      print json(['error'=>true, 'message'=>'error al copiar']);
    }
  }

}
