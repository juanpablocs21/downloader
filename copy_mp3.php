<?php
set_time_limit(0);
require 'config.php';
require 'libs/helpers.php';

function create_folder($folder){
	mkdir($folder, 0777, true);
}
function copiar($f){
	$desde = "http://i.imgur.com/du28PiA.jpg";
	$hacia = $f."/file_".rand(1,999).".jpg";
	copy($desde,$hacia);
	echo "copiado";
}

$folder = "./music";
$bucket = $folder."/bucket0";
$max_mp3 = 3;
$count_folder = count(scan_dir($folder));

if($count_folder<1){
	$f = $bucket.($count_folder+1);
	create_folder($f);
	copiar($f);
}else{
	$c = false;
	foreach(scan_dir($folder) as $bc){
		$folder_actual = "$folder/$bc";
		if(count(scan_dir($folder_actual))<=$max_mp3){
			copiar($folder_actual);
			$c = false;
			break;
		}else{
			$c = $folder_actual;
		}
	}
	if($c){
		$f = $bucket.(substr($c,-1)+1);
		create_folder($f);
		copiar($f);
	}
}