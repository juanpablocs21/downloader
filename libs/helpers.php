<?php
function hash_params($hash){

}
function json($arr){
  return json_encode($arr);
}
function my_encode($r){
  return base64_encode($r);
}
function my_data($hash){
  return ['source'=>substr($hash, 0, 2), 'id'=>substr($hash, 2)];
}

function generate_secure_hash($path, $name){
  $expire = time()+MAX_SECONDS_MP3;
  $md5 = base64_encode(md5(HASH_SECRET . $path . $expire, true));
  $md5 = strtr($md5, '+/', '-_');
  $md5 = str_replace('=', '', $md5);
  return sprintf('%s?signature=%s&e=%s&name=%s', $path, $md5, $expire, str_replace(' ','_',$name));
}

function get_server($n){
  $a['go'] = 'goear';
  $a['sc'] = 'soundcloud';
  return $a[$n];
}

function scan_dir($dir){
  return array_diff(scandir($dir), array('..', '.'));
}

function folder_exist($folder){
  $path = realpath($folder);
  return ($path !== false AND is_dir($path)) ? $path : false;
}

function count_max($folder){
  global $limit;
  $total_mp3 = count(scan_dir($folder));
  return $total_mp3>$limit;
}
function sizeFilter( $bytes )
{
    $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
    for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
    return( round( $bytes, 2 ) . " " . $label[$i] );
}
function _filesize($file){
  var_dump(number_format($file,2));
  die('xx');
  $bytes = strpos($file,'/')!==false ? filesize($file) : $file;
  if ($bytes >= 1073741824) {
      return number_format($bytes / 1073741824, 2) . ' GB';
  } elseif ($bytes >= 1048576) {
      return number_format($bytes / 1048576, 2) . ' MB';
  } elseif ($bytes >= 1024) {
      return number_format($bytes / 1024, 2) . ' KB';
  } elseif ($bytes > 1) {
      return $bytes . ' bytes';
  } elseif ($bytes == 1) {
      return '1 byte';
  } else {
      return '0 bytes';
  }
}

function buffer_mp3($url){
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url); //set url
  curl_setopt($ch, CURLOPT_REFERER, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //do not show in browser the response
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); //follow any redirects
  curl_exec($ch);
  $buffer = curl_exec($ch);
  curl_close($ch);
  return $buffer;
}

function copy_mp3_server($server,$post){
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $server);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
  $res = curl_exec($ch);
  curl_close ($ch);
  return $res;
}

function remote_filesize($url)      
{
  ob_start();
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_HEADER, 1);
  curl_setopt($ch, CURLOPT_NOBODY, 1);
  curl_setopt($ch, CURLOPT_REFERER, $url);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_exec($ch);
  curl_close($ch);
  $head = ob_get_contents();
  ob_end_clean();
  preg_match('/Content-Length:\s([0-9].+?)\s/', $head, $match);   
  return (int)@$match[1];
} 

function getLocation($url){
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url); //set url
  curl_setopt($ch, CURLOPT_REFERER, $url);
  curl_setopt($ch, CURLOPT_NOBODY, true); //do not include response body
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); //follow any redirects
  curl_exec($ch);
  $lnk = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); //extract the url from the header response
  curl_close($ch);
  return $lnk;
}

function getRemoteFilesize($url)
{
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_REFERER, $url);
  curl_setopt($ch, CURLOPT_NOBODY, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_exec($ch);
  $bytes = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
  curl_close($ch);
  return (int)$bytes;
}

function getFileUrlAndSize($url)
{
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_REFERER, $url);
  curl_setopt($ch, CURLOPT_NOBODY, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_exec($ch);
  $lnk    = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
  $bytes  = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
  curl_close($ch);
  return ['size'=>(int)$bytes, 'url'=>$lnk];
}

function convert_to_mb($bytes){
  return round($bytes / 1048576, 2);
}

