<?php
set_time_limit(0);
require 'config.php';
require 'libs/helpers.php';

function create_folder($folder){
	mkdir($folder, 0777, true);
}
function copiar($urlSource, $file){
	if(copy($urlSource, $file)){
		print json_encode(['status'=>true, 'url'=>$file, 'size'=>filesize($file), 'save_mongo'=>true]);
	}else{
		print json_encode(['status'=>false, 'url'=>'', 'save_mongo'=>false]);
	}
}

$id 				= $_POST['id'];
$source			= $_POST['source'];

switch ($source) {
	case 'go':
		$data = getFileUrlAndSize("http://www.goear.com/action/sound/get/{$id}");
		break;
	
	default:
		die(json(['status'=>false, 'message'=>'source not permited']));
		break;
}

$mbSource 	= convert_to_mb($data['size']);
$urlSource 	= $data['url'];

if($mbSource>12){
	print json_encode(['status'=>true, 'url'=>$urlSource, 'size'=>$data['size'], 'save_mongo'=>false]);
	die;
}

$folder = "./music";
$bucket = $folder."/bucket0";
$max_mp3 = 3;
$count_folder = count(scan_dir($folder));
$name_mp3 = "{$source}_{$id}.mp3";

if($count_folder<1)
{
	$f = $bucket.($count_folder+1);
	create_folder($f);
	copiar($urlSource, "{$f}/{$name_mp3}");
}
else
{
	$c = false;
	foreach(scan_dir($folder) as $bc){
		$folder_actual = "$folder/$bc";
		if(count(scan_dir($folder_actual))<=$max_mp3){
			copiar($urlSource, "{$folder_actual}/{$name_mp3}");
			$c = false;
			break;
		}else{
			$c = $folder_actual;
		}
	}
	if($c){
		$f = $bucket.(substr($c,-1)+1);
		create_folder($f);
		copiar($urlSource, "{$f}/{$name_mp3}");
	}
}
